package main

import (
	"errors"
	"net/http"
)

//func serve(r chi.Router) {
//r.Post("/", func(w http.ResponseWriter, r *http.Request) {

//}
//}

var (
	// ErrMissingAuthentication is returned when passhash or memberid is missing
	ErrMissingAuthentication = errors.New("Username or memberid missing")
)

// AuthenticateHTTP ..
func AuthenticateHTTP(w http.ResponseWriter, r *http.Request) {
	var (
		passhash  = r.FormValue("passhash")
		memberid  = r.FormValue("memberid")
		sessionid string
	)

	if passhash == "" || memberid == "" {
		onError(ErrMissingAuthentication, &w)
		return
	}

	req, err := http.NewRequest(
		"POST",
		"https://forums.e-hentai.org/index.php",
		nil,
	)

	if err != nil {
		onError(err, &w)
		return
	}

	req.Header.Set("Cookie", "ipb_pass_hash=%s; ipb_member_id=%s")

	resp, err := (&http.Client{}).Do(req)
	if err != nil {
		onError(err, &w)
		return
	}

	for _, cookie := range resp.Cookies() {
		if cookie.Name == "ipb_session_id" {
			sessionid = cookie.Value
		}
	}

	if sessionid == "" {
		onError(ErrMissingAuthentication, &w)
		return
	}

	cookies := []*http.Cookie{
		&http.Cookie{
			Name:   "ipb_member_id",
			Value:  memberid,
			Path:   "/",
			Domain: "e-hentai.org",
		},
		&http.Cookie{
			Name:   "ipb_pass_hash",
			Value:  passhash,
			Path:   "/",
			Domain: "e-hentai.org",
		},
		&http.Cookie{
			Name:   "ipb_member_id",
			Value:  sessionid,
			Path:   "/",
			Domain: "e-hentai.org",
		},
	}

	for _, c := range cookies {
		http.SetCookie(w, c)
	}

	w.Header().Add("Location", "/")
	w.WriteHeader(200)
}
